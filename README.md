# January 20
I made the initial post explaining my idea about the project smart dice and how it could be a six sided display dice which could be programmed to have any number of possiblities. The idea was well recieved on the web board.  

**Figure 1** a similar device to my idea

# January 23
I pitched the dice to get approval by telling how i planned to make different features like using an app to teach people about probebility as one could vary gaussian curves to see how they impacted their throws also using a potentiometer to figure out which side of the dice was landing at the top and using that to give a result to the other sides as well. 

# January 26
Completed safety training by following the instructional video and printing and ulpoading my certificate. 

# January 28
Completed CAd assignment and learned how to work with KiCAD. 

# February 8
Attended the first TA meeting and we talked about our various ideas. 

# February 11
Submitted a proposal with my group members to build a carbon dioxide sensing wall mounted device. I worked on figuring out how to build an alarm system for it.  

# February 21
We drafted a basic design document and showed it to our TA during the TA meeting and recieved feedback. 

# February 22
I started working on the soldering assigment in lab. I wasn't able to get my first board to work and had to get a new kit and start again. 

# February 23 
Finally i was able to get the soldering assigment done. 

# February 24
Completed design document by workig on the design for the alarm subsystem i utilized various soures to understand how the alarm needs to be designed using both an programmable LED and a speaker that warn using Audio. The speaker was going to be attached to an amplifier whose gain in sound could be adjusted. I also worked on compiling and citing the references, the ethics and safety considerations and also the Schedule of workflow. 

# February 28
We went over the design document and presented it to a professor who gave us positive feedback on our design for the project.  

# March 3
Started working on a PCB design that was eventually not used due to some of its connections being less than ideal. This made me moe onto desiging a new PCB. 

# March 20
I managed to work on the schematic and design the alarm subsysystem schematic. I first designed how the MAX3687A needs to be connected in the schematic and where the I2S signals from the MCU are supposed to be recieved in. After that i found out whih pins need to be shorted and disable the SD_MODE pin. I also designed how the gain would work in our circuit and how we would utilise only three gain settings and can use the unconnected pin for the other two gain settings. I also figured out how the output should be routed to the speaker. 

The image for the MAX3678 and alarm subsystem schematic is the **figure 2** uploaded. 

# March 25
I had another TA meeting with daniel where we updated him on the staus of the PCB design we were working on and how we had already send the board for fabrication and when we will be recieving it. 

# March 27
I figured out how the MCU breakout board was to be programmed with the Arduino IDE by trying to run the arduino blink program on the GPIO 23 pin on the ESP32. This was first accomplished b downloading the ESP32 library by expressif onto the arduino IDE. Then changing the board to the ESP32 in arduino and then programming it. 

`
void setup() {
  
  pinMode(23, OUTPUT);
}
void loop() {
  digitalWrite(23, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(23, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
`

I also figured out how to collect the CO2 data using the MCU by downloading the sensirion libraries and running the example code they had for running the CO2 sensor and collecting its data. This was acocmplished by sending a commnad of start_periodic_measurement to begin sensor data collection, then continuing to collect data using the get_measuremnt commnad. The data read could be ended by sending the stop_periodic_measurement command to the x62 address on the SCD-41
The reference for the SCD-41 development is : https://learn.adafruit.com/adafruit-scd-40-and-scd-41/arduino

The **figure 3** corresponds to the graph of the CO2 concentration that the SCD-41 sensor generated  

# March 30
I worked on my individual progress report where i explained my contributions to the project in the ways of the alarm subsystem and getting the sensor to work. I also explained how i planned to test the speaker and led components in the future.  

# March 31
I started working on the soldering of the first board by helping apply the paste through the stencil. Secondly i started doing rot cause analysis for the failure of our initial sensor. This was acomplished by sending commands like perform_fatory_reset or forced_calibration to make sure that the sensor could be made to reset to its intial level. Succefully did continuity checks on the soldered PCB. 

# April 1
We discussed the issue of our sensor not working with our TA daniel. And how we planned to solve the issue by figuring ot what went wrong and then resetting it or starting over with a new sensor. 

# April 2
I tried to debug the sensor by first trying to establish communication with the sensor and trying to send instruction to recalibarate it if the connection was made. I also had hooked up the sensor to the oscilloscope and was performing measurements to understand how the sensor was communicating. 
`
#include <Arduino.h>
#include <SensirionI2CScd4x.h>
#include <Wire.h>

SensirionI2CScd4x scd4x;
void setup() {

    Serial.begin(115200);
    while (!Serial) {
        delay(100);
    }

    Wire.begin();

    uint16_t error;
    char errorMessage[256];

    scd4x.begin(Wire);

    //  started measurement
    error = scd4x.startPeriodicMeasurement();
    if (error) {
        Serial.print("Error trying to execute startPeriodicMeasurement(): ");
        errorToString(error, errorMessage, 256);
        Serial.println(errorMessage);
        scdx.forcedrecalibrate();
    }

   }

void loop() {
    uint16_t error;
    char errorMessage[256];

    delay(5000);

    uint16_t co2;
    error = scd4x.readMeasurement(co2);
    if (error) {
        Serial.print("Error trying to execute readMeasurement(): ");
        errorToString(error, errorMessage, 256);
        Serial.println(errorMessage);
    } else if (co2 == 0) {
        Serial.println("Invalid sample detected, skipping.");
    } else {
        Serial.print("Co2:");
        Serial.print(co2);
        }
}
`

# April 4
I tried to decode the sensor using the oscilloscope in the lab and couldn't find any proof of the sensor recieving and sending back any intelligent data. 

The **figure 5** is te SCl signal returned 
The **figure 6** is the SDa signal returned

# April 7
I continued using the oscilloscope to decode the sensor and came to the conclusion that the sensor was beyond repair and there was no way to establish communication with the sensor. This made me decide to utilise a new sensor. 

# April 8
In the TA meeting i explained what i found scoping and trying to decode the sensor. And how i was unsuccessful in trying to communicate with the sensor by being unable to send any instruction. 

# April 9
To run the PIR sensor using the ESP32 dev board we used the arduino IDE and connected the pins of the PIR to the PCB. The output pin of PIR is connected to a 10k pull down resistor. This output is then fed into the GPIO 18 of esp32   
`
void setup() {
  Serial.begin(9600);
  pinMode(18,OUTPUT);
}

void loop() {
  
  if (digitalRead(PIR) == HIGH) {
    Serial.println("motion detected"); 
  } else {
    Serial.println("motion not detected");  
  }
  

}`

**Image 7** shows the PIR sensor output 

# April 11
I started working with the PC to get the alarm components working. I managed to upload he code of a blinking led to the GPIO 19 pin used for the flora led to make a normal led blink on the breadboard using the PCB. This proved that the code could be uploaded during reset. I had trouble uploading code further but was bale to note down the voltage value of the rails in the PCB. 

# April 12
Figured out how to program the board by putting it in bootloader mode. This was accomplished by shorting both the GPIO0 (BOOT) and the reset pin on the ESP32. This sent our board into bootloader mode and we were able to upload a program form our terminal into the flash of the ESP32. 

I also mamaged to test the speaker using the PCB and help from a source of prior work in this area : https://diyi0t.com/i2s-sound-tutorial-for-esp32/

i had to change my pinout but was able to output an audio file onto the Speaker which was attached to the PCB. 


`
#include "AudioGeneratorAAC.h"
#include "AudioOutputI2S.h"
#include "AudioFileSourcePROGMEM.h"
#include "sampleaac.h"
AudioFileSourcePROGMEM *in;
AudioGeneratorAAC *aac;
AudioOutputI2S *out;
void setup(){
  Serial.begin(115200);

  in = new AudioFileSourcePROGMEM(sampleaac, sizeof(sampleaac));
  aac = new AudioGeneratorAAC();
  out = new AudioOutputI2S();
  out -> SetGain(0.125);
  out -> SetPinout(26,25,33);
  aac->begin(in, out);
}

void loop(){
  if (aac->isRunning()) {
    aac->loop();
  } else {
    aac -> stop();
    Serial.printf("Sound Generator\n");
    delay(1000);
  }
}
`



This code outputs an audio file. 

# April 14
I was able to program the flora LED using the GPIO 19 pin. This code outputs a green light. 

`
#include <Adafruit_NeoPixel.h>
#define PIN        19 // On Trinket or Gemma, suggest changing this to 1
#define NUMPIXELS 500 


void setup() {
  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
}

void loop() {
  pixels.clear(); // Set all pixel colors to 'off'

  for(int i=0; i<NUMPIXELS; i++) { // For each pixel...

    pixels.setPixelColor(i, pixels.Color(0, 150, 0));

    pixels.show();   // Send the updated pixel colors to the hardware.

    delay(500); // Pause before next pass through loop
  }
}
`


I was also able to confirm that the battery when fully charged depicts green light (**figure 8**) and a yellow light while in the process of charging (**figure 9**)

# April 15
In this Ta meeting we explained how we had successfully gotten our alarm components to work and our PCB design worked prefectly. 

# April 18
Tried to make the speaker output a beep sound instead of the default sound using a wave file which was converted into a hex array and then read and outputed through the I2S bus. This approach wasn't successful as the hex file could be corrupted by noisy data. 

# April 19
During the first itegration attempt tried to make the PIR sensor work together with the LED and reduce the fluctuations that ocurred were to be fixed using a delay. This was an unsuccesful attempt but the isue was solved on its own later on. 

# April 21
Wrote the initial approach to the ACH code using Flask for the server subsystem this was to calculate the SCH quantity -ln(C1/C0)/(t1-t0). Only when the conditions were met did the server calculate and display the ACH. The conditions being a time difference of at least 30 seconds and continuous false values for at least 6 entries and lastly a Co2 decrease by 20 percent at least. 

`
def string_to_bool(string):
    return bool(strtobool(str(string)))

class data(db.Model):
    '''Database Schema'''
    # ID is integer, serves as primary key, unique id per reading
    id = db.Column(db.Integer, primary_key=True)
    # CO2 reading is an integer representing CO2 value
    co2 = db.Column(db.Integer, unique=False, nullable=False)
    # Occupancy (integer for now, maybe bool)
    occupancy = db.Column(db.Boolean, unique=False, nullable=False)
    # Time as a datetime python onject
    time = db.Column(db.DateTime, nullable=False)
    # # Alarm status represents the node's current alarm status, i.e. Green, Yellow, Red, and global
    # alarm_stat = db.Column(db.String(50), unique=False, nullable = False)
    # # Sensor ID, unique for each device, set on the device
    # sensor_id = db.Column(db.Integer, unique=True, nullable=False)
    # # Room ID, set on devid
    # room_id = db.Column(db.Integer, unique=False, nullable=False)
    # # Temp and humidity
    # temp = db.Column(db.Integer, unique=False, nullable=False)
    # humidity = db.Column(db.Integer, unique=False, nullable=False)

    def __repr__(self):
        # {self.room_id} by sensor {self.sensor_id} \n alarm status is {self.alarm_stat}"
        return f"CO2 was {self.co2} with {self.occupancy} occupancy in room"


@app.route('/', methods=['POST', 'GET'])
def index():
    print(request.method == 'POST')
    if request.method == 'POST':
        # parse data
        jsonData = request.data.decode('utf-8')
        jsonData = json.loads(jsonData)

        # extract values from JSON
        co2 = jsonData["co2"]
        occupancy = string_to_bool(jsonData["occupancy"])
        time = jsonData["time"]
        # careful with the format string
        dt_object = datetime.fromtimestamp(time)
        print("dt_object")
        
        # make new entry
        
        new_data = data(co2=co2, occupancy=occupancy, time=dt_object)
        
        try:
                # try to add to database
            db.session.add(new_data)
            db.session.commit()
            return redirect('/')
        except BaseException as e:
            print(e)
            return "error!"
    else:
        tasks = data.query.order_by(data.time).all()
        # print(tasks)
        return render_template("index.html", tasks=tasks)
    
@app.route('/curve', methods=['GET', 'POST'])
def curve():
    ach_data = data.query.order_by(data.time).with_entities(
        data.time, data.co2, data.occupancy).all()
    ach_data = list(zip(*ach_data[0:]))
    sec = ach_data[0]
    carbon = ach_data[1]
    here = ach_data[2]
    #sec = [datetime(2022, 4, 21, 22, 25, 50), datetime(2022, 4, 21, 22, 26, 26), datetime(2022, 4, 21, 22, 26, 54),datetime(2022, 4, 21, 22, 27, 30), datetime(2022, 4, 21, 22, 27, 54),datetime(2022, 4, 21, 22, 28, 10), datetime(2022, 4, 21, 22, 28, 54)]
    #carbon = [1205,200, 700, 800, 900, 1100, 1120]
    #here = [True, False, False, False, True, False, True]
    x = 1
    conc = 0
    ACH = 0

    while True :
        j = x
        while here [-j] != False:
            j = j +1
        x = j
        while here[-(x+1)] != True:
            x = x+1
        
        c = -(carbon[-x]-carbon[-j])
        b = (sec[-j]-sec[-x])
        t= (sec[-j]-sec[-x]).seconds
        if c >= carbon[-x]*0.2:
           if t >= 30:
               ACH = np.log(carbon[-j]/carbon[-x])/t
               break
           else:
               x = x + 1
               ACH = -1
        else:
            x = x + 1
            ACH = -1
    return  f"ACH was {ACH} for concen {c}, {carbon[-x]}, {carbon[-j]}  time {t} , {b} , {sec[-j]}, {sec[-x]}"



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
`




**This ACH model was discarded later on for a more optimized system**

 Also centered and organized how the PCB would be mounted and the holes for the access and mounting for the various componennts will work by putting tape and marking where the holes needed to be drilled. 

 **Figure 10** shows the final enclosure drilled with the drilled holes for PIR, Co2 sensor and LED. 

# April 22
Mock demo where we showed our system changing colors and outputing the sound when the CO2 concentration reached over a threshold value of 1400ppm. This prooved our system worked according to our design and that our project was working according to claims. 

# April 23
Verified that the device could work and collect data for 8 hours and that the battery could power the device for that period of time at least. 

**Figure 11** shows the voltage across a charged battery
**Figure 12** shows the battery voltage after 8 hours. 


# April 24
For our final demo we needed a presentation showing all the various susbsytems being verified working. I created a presentation will all of those videos to show in case the final demo did not go as planned and our project did not work. I also compiled the final requirements and verification tables. 

# April 25
Final demo was performed and we walked our Ta and the professor through our working model. We showed the CO2 concentration data, the ACH and the tabulated data on the servr system. As well as the alarm system working with the three led levels and the speaker output as well. 

# April 28 
We prepared our slides and got feedback on the mock presentation. 

# May 1
Started working on preparing material for the final report. I worked on the technical details of the MCU, PIR, CO2 , Flora LED and the alarm subsystem. 

# May 4 
Finished presentation with my group members and got a good response. I covered the final results including the verification as well as the conclusion and future works. 

# May 5 
Finished working on the report with my group members. I worked on the appendix which consisted of the schematic and the requirement and verification tables. The references, costs, conclusion as well as subsystems like alarm and all the verification images were finalized by me.

# May 6
Finished lab checkout. 
